from typing import Optional, List
from pydantic import BaseModel, Field, validator
from datetime import date, datetime


class Ontology(BaseModel):
    """ Ontology Schema """
    id: int = Field(..., ge=1)
    description: str = Field(..., max_length=250)
    # root_code_ids: list[int] = Field(..., max_length=200)


class Code(BaseModel):
    """ Code Schema """
    code: str = Field(..., max_length=10)
    description: str = Field(..., min_length=1, max_length=2048)
    depth: int = Field(..., ge=1)
    path: str = Field(..., min_length=6)
    children: Optional[List[str]]
    updated_at: date
    release_year: int = Field(..., ge=2020)
    # id: int = Field(..., ge=1)
    # ontology_id: int = Field(..., ge=1)
    # added_at: date = Field(..., ge='2015-01-01')
    # removed_at: Optional[date] = Field(..., ge='2015-01-01')

    @validator('updated_at')
    def date_greater_that(cls, updated_at):
        if updated_at > datetime.today().date():
            raise ValueError("updated_at must be less than today's date")
        return updated_at

