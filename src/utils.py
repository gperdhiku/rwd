import pandas as pd
from pydantic.main import ModelMetaclass
from typing import List
from pydantic import BaseModel
from psycopg2 import OperationalError


def validate_data_schema(data_schema: ModelMetaclass):
    """This decorator will validate a pandas.DataFrame against the given data_schema."""

    def Inner(func):
        def wrapper(*args, **kwargs):
            res = func(*args, **kwargs)
            if isinstance(res, pd.DataFrame):
                # check result of the function execution against the data_schema
                df_dict = res.to_dict(orient="records")

                # Wrap the data_schema into a helper class for validation
                class ValidationWrap(BaseModel):
                    df_dict: List[data_schema]

                # Do the validation
                _ = ValidationWrap(df_dict=df_dict)
            else:
                raise TypeError("Your Function is not returning an object of type pandas.DataFrame.")

            # return the function result
            return res

        return wrapper

    return Inner


def get_children_nodes(node):
    """ will get all elements from the xml file which has a tag equal to "chapter", "section" or "diag" """
    children_nodes = [elem for elem in list(node) if elem.tag in ["chapter", "section", "diag"]]
    return children_nodes


def get_chapter_code_from_desc(desc):
    """ extract chapter code from its description """
    return desc[desc.find("(") + 1:desc.find(")")]


def get_node_code(node):
    """ extract code from elements with tag equal to "chapter", "section" or "diag" """
    if node.tag == "section":
        name_text = node.attrib["id"]
    elif node.tag == "chapter":
        desc_l = [elem for elem in list(node) if elem.tag in ["desc"]]
        desc = next(iter(desc_l or []), None)
        name_text = get_chapter_code_from_desc(desc.text)
    elif node.tag == "diag":
        name_l = [elem for elem in list(node) if elem.tag in ["name"]]
        name = next(iter(name_l or []), None)
        name_text = name.text
    else:
        name_text = ""

    return name_text


def get_node_desc(node):
    """ extract description from elements with tag equal to "chapter", "section" or "diag" """
    if node.tag in ["section", "chapter", "diag"]:
        desc_l = [elem for elem in list(node) if elem.tag in ["desc"]]
        desc = next(iter(desc_l or []), None)
        desc_text = desc.text
    else:
        desc_text = "empty"

    return desc_text


def executeScriptsFromFile(filename, cursor):
    """ parse file and execute all sql commands """
    # Open and read the file as a single buffer
    fd = open(filename, 'r')
    sqlFile = fd.read()
    fd.close()

    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')

    # Execute every command from the input file
    for command in sqlCommands:
        # This will skip and report errors
        try:
            cursor.execute(command)
        except OperationalError as err:
            print("Command skipped: ", err)


def operation(node, depth=0, path=""):
    """
    it wii create an array/row to be inserted in the pandas dataframe
    anytime it visits a node recursively in the tree structured object (xml file)
    """

    if depth == 0:
        text = "start"
        row_arr = None
    else:
        children_codes = [get_node_code(elem) for elem in get_children_nodes(node)]
        children_codes = "[" + ', '.join(f'"{c}"' for c in children_codes) + "]"
        text = f'{get_node_code(node)}: {get_node_desc(node)}; depth:{depth}, path: "{path}", children_ids: {children_codes}'
        row_arr = [get_node_code(node), get_node_desc(node), depth, path, children_codes]
    # print(text)
    return row_arr


