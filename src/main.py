import pandas as pd
import xml.etree.ElementTree as ET
import utils
from utils import validate_data_schema, executeScriptsFromFile
import schema
from ast import literal_eval
import argparse
import psycopg2
from sqlalchemy import create_engine
from datetime import datetime


def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'


parser = argparse.ArgumentParser()
parser.add_argument("--file_path", help="Path of the xml file to load", type=str, required=True)
parser.add_argument("--year", help="year of release of ICD-10-CM", type=int, required=True)
parser.add_argument("--initial_load", help="True if it is the first time loading the data", type=boolean_string,
                    required=True)

args = parser.parse_args()

year = args.year
tab = []


def import_xml_file(file_path):
    """ Read xml file and create an array (of arrays) as output which contains the rows to be inserted in the
    dimension table.
    """
    tree = ET.parse(file_path)
    root = tree.getroot()

    def recur_node(node, f, d=0, path=""):
        """Applies function f on given node and goes down recursively to its
           children.

           Since we do not know the depth of the tree (could be "infinite" <diag> elements inside a <diag> element),
            we will make use of a recursive function to scan and visit all elements.
        """
        global tab

        if node is not None:
            row = f(node, d, path)
            if row:
                tab.append(row)
            for item in utils.get_children_nodes(node):
                recur_node(item, f, d + 1, path + "/" + utils.get_node_code(item))
        else:
            return 0

    recur_node(root, utils.operation)


@validate_data_schema(data_schema=schema.Code)
def transform() -> pd.DataFrame:
    """ transform the array we get from previous step (import_xml_file) by adding more information,
    and converting it to a pandas df.
    Validate the dataframe by using the validate_data_schema decorator
    """
    global tab
    global year

    tab_df = pd.DataFrame(tab)
    # update df column names
    tab_df.columns = ["code", "description", "depth", "path", "children"]

    # Convert children string column to array of strings
    tab_df.children = tab_df.children.apply(literal_eval)

    # add updated_at and year columns
    tab_df["updated_at"] = datetime.today()
    tab_df["release_year"] = year
    return tab_df


def load_to_db(df, initial_load):
    """ Loads a pandas df to the postgress df"""

    # cloud db connection details
    database = "dc2j9bg9b8pdvd"
    user = "xbjflscgysmmat"
    password = "b66123c19a7d1667d698b1be062852ac0f03ec45e22129b55d8042573f445e1a"
    host = "ec2-52-208-164-5.eu-west-1.compute.amazonaws.com"
    port = "5432"

    # local db connection details
    # database_local = "postgres"
    # user_local = "g.perdhiku"
    # host_local = "localhost"
    # port_local = "5432"

    # connect
    try:
        conn = psycopg2.connect(database=database, user=user,
                                password=password,
                                host=host, port=port)

        # conn = psycopg2.connect(database=database_local, user=user_local,
        #                         host=host_local, port=port_local)

    except:
        print("I am unable to connect to the database")

    conn.autocommit = True
    cursor = conn.cursor()

    if initial_load:
        # create initial tables
        executeScriptsFromFile("sql/create_tables.sql", cursor)

    # Insert code data from pandas df into postgres table -  using sql alchemy only

    connect = "postgresql+psycopg2://%s:%s@%s:5432/%s" % (
        user,
        password,
        host,
        database
    )

    # connect = "postgresql+psycopg2://%s:%s@%s:5432/%s" % (
    #     user_local,
    #     "",
    #     host_local,
    #     database_local
    # )

    engine = create_engine(connect)
    df.to_sql(
        'code',
        con=engine,
        index=False,
        if_exists='append'
    )
    conn2 = engine.raw_connection()
    conn2.commit()
    conn2.close()

    # Create final dimension dim_code
    executeScriptsFromFile("sql/dim_code.sql", cursor)

    # close db connection and the cursor
    conn.close()
    cursor.close()


if __name__ == "__main__":
    # parse xml file
    import_xml_file(file_path=args.file_path)

    # transform and validate df
    df = transform()

    # load data into postgres db
    load_to_db(df=df, initial_load=args.initial_load)

    print("Process completed successfully ...")
