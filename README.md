# RWD



## Getting started

Run the following commands to create a python virtual environment and install all the needed requirements

```
python3 -m pip install --user virtualenv
python3 -m venv env
source env/bin/activate
python3 -m pip install -r requirements.txt
```

Run mypy for static type checker

```
mypy src/main.py
```

## Run process
Load xml file for 2020

```
python src/main.py --file_path="files/icd10cm_tabular_2020.xml" --year=2020 --initial_load=True
```

Load xml file for 2021

```
python src/main.py --file_path="files/icd10cm_tabular_2021.xml" --year=2021 --initial_load=False
```

## Connect to postgres and read from dim_code dimension table.

Install any sql client (like dbeaver etc.)
Connect to db using the following details:

```
database = "dc2j9bg9b8pdvd"
user = "xbjflscgysmmat"
password = "b66123c19a7d1667d698b1be062852ac0f03ec45e22129b55d8042573f445e1a"
host = "ec2-52-208-164-5.eu-west-1.compute.amazonaws.com"
port = "5432"
```


Query the table
```
select *
from public.dim_code
```

## Next steps

1. Run pre-commits validate configuration files (if any), auto-format python code (black) and check for coding style (PEP8). 
2. Build a ci/cd pipeline to run pre-commits and unit testing before committing to your branch or merging to the main branch.
3. Use a more sophisticated tool to install python requirements and to manage dependencies (like conda, pipenv etc.).
4. Use mypy to check static type of the external libraries (like pandas etc.)
5. Use an orchestration tool (like Airflow) to schedule the pipeline.
