-- script will be executed only during the initial load.
-- It will create the code nd ontology tables

DROP TABLE IF EXISTS public.code CASCADE
;
DROP TABLE IF EXISTS public.ontology CASCADE
;

CREATE TABLE IF NOT EXISTS public.code(
    id serial PRIMARY KEY
  , code varchar
  , description varchar
  , depth integer
  , path varchar
  , children varchar[]
  , updated_at timestamp
  , release_year integer
)
;

CREATE TABLE IF NOT EXISTS public.ontology(
    id serial PRIMARY KEY
  , description varchar
)
;

INSERT INTO public.ontology
values
(1,'icd10'),
(2,'icd9'),
(3,'hcps'),
(4,'ndc')