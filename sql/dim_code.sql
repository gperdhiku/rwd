-- this script will create the final code dimension table

drop table if exists public.dim_code
;

create table public.dim_code
as
with codes as (
	select id
	  , code
	  , description 
	  , depth
	  , path
	  , children
	  , array_to_string(children, ',', '*') as children_text
	  , release_year
	  , (select id from public.ontology where description = 'icd10') as ontology_id
	  -- assuning we load the files every year
	  , case when release_year = (select max(release_year) from public.code)  then null
	      else release_year+1 end as removed_at
	  , updated_at
	  -- window function to remove the duplications
	  , row_number() over (partition by code, depth order by release_year desc, updated_at desc nulls last, depth desc) as row_num
	from public.code
)
select id
  , release_year
  , code
  , description
  , depth
  , path
  , children
  , children_text
  , ontology_id
  , removed_at
  , updated_at
from codes 
where row_num = 1